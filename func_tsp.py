from tsp_solver.greedy import solve_tsp
import traceback

# https://github.com/dmishin/tsp-solver
# Suboptimal Travelling Salesman Problem (TSP) solver

def tsp(A, n_node):
    D = [[0 for i in xrange(n_node)] for j in xrange(n_node)]
    
    scale = (n_node-1)*n_node/2
    cnt = 0
    for i in xrange(0,n_node):
        D[i][i] = 0
        for j in xrange(i+1, n_node):
            D[i][j] = A[cnt]
            D[j][i] = A[cnt]
            cnt += 1
    assert cnt == scale


    try:
        path = solve_tsp( D )
        # print path            
    except BaseException as e:
        # print("******* %s stops by %s *******" % (__name__, repr(e)))
        # traceback.print_exc()
        # print 'exception'
        pass            

    return