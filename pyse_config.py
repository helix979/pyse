import ConfigParser
import pickle, os
config_in = "config.ini"
#----------------------------------------------------------------
def getEnvVar(key):

    c = ConfigParser.ConfigParser()
    c.read(config_in)

    module_name = c.get(key, 'module_name')
    func_name = c.get(key, 'func_name')
    subdir = 'results_%s' % (key)
    if not os.path.exists(subdir):
        os.mkdir(subdir)
        
    return module_name, func_name, subdir
#----------------------------------------------------------------
if __name__ == '__main__':
    print getEnvVar('isort')




