import sys

def dijkstra3(A, N):
    # static int[] runDijkstra(int N, int D[][], int src) {
    src = 0    
    int_max = sys.maxint            

    D = [[0 for i in range(0,N)] for j in range(0,N)]

    scale = (N-1)*N/2
    cnt = 0
    for i in range(0,N):
        for j in range(i,N):
            if(i==j):
                continue
            D[i][j] = A[cnt]
            D[j][i] = A[cnt]
            cnt += 1
    assert cnt == scale
    
    dist = []
    fixed = []
    for i in xrange(N):
        dist.append(int_max)
        fixed.append(False)
    dist[src] = 0
    
    for k in xrange(N):
        # Find the minimum-distance, unfixed vertex.
        min = -1
        minDist = int_max
        for i in xrange(N):
            if (not fixed[i]) and (dist[i] < minDist):
                min = i
                minDist = dist[i]
        # Fix the vertex.
        fixed[min] = True
        
        # Process the vertex's outgoing edges.
        for i in xrange(N):
            if (not fixed[i]) and (dist[min] + D[min][i] < dist[i]):
                dist[i] = dist[min] + D[min][i]
                
def dijkstra3_asym(A, N):
    # static int[] runDijkstra(int N, int D[][], int src) {
    src = 0    
    int_max = sys.maxint            

    D = [[0 for i in range(0,N)] for j in range(0,N)]

    scale = (N-1)*N
    cnt = 0
    for i in range(0,N):
        for j in range(i,N):
            if(i==j):
                continue
            D[i][j] = A[cnt]
            cnt += 1
            D[j][i] = A[cnt]
            cnt += 1
    assert cnt == scale
    
    dist = []
    fixed = []
    for i in xrange(N):
        dist.append(int_max)
        fixed.append(False)
    dist[src] = 0
    
    for k in xrange(N):
        # Find the minimum-distance, unfixed vertex.
        min = -1
        minDist = int_max
        for i in xrange(N):
            if (not fixed[i]) and (dist[i] < minDist):
                min = i
                minDist = dist[i]
        # Fix the vertex.
        fixed[min] = True
        
        # Process the vertex's outgoing edges.
        for i in xrange(N):
            if (not fixed[i]) and (dist[min] + D[min][i] < dist[i]):
                dist[i] = dist[min] + D[min][i]
