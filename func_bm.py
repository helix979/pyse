
def make_delta1(delta1, pat, patlen):

    for i in range(patlen):
        delta1[pat[i]] = patlen-1 - i



def is_prefix(word, wordlen, pos):
    suffixlen = wordlen - pos    
    for i in range(suffixlen):
        if (word[i] != word[pos+i]):
            return 0
    return 1


def suffix_length(word, wordlen, pos):

    i = 0
    while (word[pos-i] == word[wordlen-1-i]) and (i < pos):
        i += 1
    return i;

def make_delta2(delta2, pat, patlen):
    
    last_prefix_index = patlen-1    
    for p in reversed(range(patlen)):
        if is_prefix(pat, patlen, p+1):
            last_prefix_index = p+1

        delta2[p] = last_prefix_index + (patlen-1 - p)
    

    for p in range(patlen):
        slen = suffix_length(pat, patlen, p)
        if (pat[p - slen] != pat[patlen-1 - slen]):
            delta2[patlen-1 - slen] = patlen-1 - p + slen
        
def use(delta, char, patlen):
    if delta.has_key(char):
        return delta[char]
    else:
        return patlen


def boyer_moore(string, pat):
    stringlen = len(string)
    patlen = len(pat)

    delta1 = {}
    delta2 = [0 for i in range(patlen)]

    make_delta1(delta1, pat, patlen)
    make_delta2(delta2, pat, patlen)
    # print delta1
    
    if (patlen == 0):
        return string

    i = patlen-1
    while (i < stringlen):
        j = patlen-1
        while (j >= 0 and (string[i] == pat[j])):
            i -= 1
            j -= 1

        if (j < 0):
            return string[(i+1):]

        i += max(use(delta1, string[i], patlen), delta2[j])
        
    

    return 0
    
def boyer_moore_wrapper(T, aux = None):
    m = boyer_moore(T, [1,2,3])
    return 
    
if __name__ == '__main__':
    # print boyer_moore('abcde','cd')
    print boyer_moore([1,2,3,4],[2,3])