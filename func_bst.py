
class Node:
    def __init__(self, val):
        self.l = None
        self.r = None
        self.v = val

class Tree:
    def __init__(self):
        self.root = None

    def getRoot(self):
        return self.root

    def add(self, val):
        if(self.root == None):
            self.root = Node(val)
        else:
            self._add(val, self.root)

    def _add(self, val, node):
        if(val < node.v):
            if(node.l != None):
                self._add(val, node.l)
            else:
                node.l = Node(val)
        else:
            if(node.r != None):
                self._add(val, node.r)
            else:
                node.r = Node(val)

    def find(self, val):
        if(self.root != None):
            return self._find(val, self.root)
        else:
            return None

    def _find(self, val, node):
        if(val == node.v):
            return node
        elif(val < node.v and node.l != None):
            self._find(val, node.l)
        elif(val > node.v and node.r != None):
            self._find(val, node.r)

    def printTree(self):
        if(self.root != None):
            self._printTree(self.root)

    def _printTree(self, node):
        if(node != None):
            self._printTree(node.l)
            print str(node.v) + ' '
            self._printTree(node.r)

#-------------------------------------------------------            
def build_and_find(numbers):

    t = Tree()

    for elm in numbers:
        t.add(elm)
        
    node = t.find(numbers[-1])    
    return node
#-------------------------------------------------------            
def build(numbers):

    t = Tree()

    for elm in numbers:
        t.add(elm)
    return
#-------------------------------------------------------    
#-------------------------------------------------------    
def build_and_find_wrapper(numbers, aux = None):

    return build_and_find(numbers)
#-------------------------------------------------------    
def build_wrapper(numbers, aux = None):

    return build(numbers)    
#-------------------------------------------------------        
    
if __name__ == '__main__':
    import numpy as np
    
    N = 10
    input = np.random.randint(N, size=N)
    
    t = Tree()
    for elm in input:
        t.add(elm)
        
    t.printTree()            
    
    val = input[0]
    print t.find(val)