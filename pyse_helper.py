import sys
import re
import os
import numpy as np
import pickle

################################################################################ 
def minsec(sec):
    min = int(int(sec) / 60)
    s = int(sec) - min*60
    return str(min) + ':' + str(s)
################################################################################    
def scale(subdir, e):    
    fname = os.path.join(subdir, 'loc_dic.dat')
    if os.path.isfile(fname):
        with open(fname, 'rb') as fdat:
            loc_dic, loc_cnt = pickle.load(fdat)    
    else:
        loc_dic, loc_cnt = {}, 0
        
    # print "before: ", [e[0], e[3]]
    
    states = []
    for s in [e[0], e[3]]:
        if s is None:
            states.append(None)
            break
        for i in range(len(s)):
            loc = s[i, 1]
            if (loc > 0):
                if (not loc_dic.has_key(loc)):
                    loc_dic[loc] = loc_cnt
                    print "A new branch location registered: ", loc_cnt, loc
                    loc_cnt += 1                
                s[i, 1] = loc_dic[loc]
            s[i, 0] = s[i, 0]/5.
        states.append(s)
    e[0] = states[0]
    e[3] = states[1]
    e[2] = e[2]/5.
    # print "e: ", e
    # print "after: ", [e[0], e[3]]
    
    with open(fname, 'wb') as fdat:
        pickle.dump([loc_dic, loc_cnt], fdat) 
    
    return e
################################################################################    
def parse(subdir, scaling = True):
    filename = os.path.join(subdir, 'path_log.dat')
    with open( filename, "rb" ) as f:
        trace = pickle.load(f)
    
    experience = []
    l = len(trace)    
    for i in xrange(l):        
        state = trace[i][0]
        action = trace[i][1]
        feasible = trace[i][3]
        if i+1 == l:
            state_new = None
            reward = 1
            if feasible == 0:
                reward = -20
            e = [np.copy(state), action, reward, state_new]                
        else:
            state_new = trace[i+1][0]
            reward = 1
            e = [np.copy(state), action, reward, np.copy(state_new)]
        e = scale(subdir, e)
        experience.append(e)    
        # print state, action, reward, state_new, '\n'
        
    return experience
################################################################################    
def new_exp(hashtable, exp):
    
    new_exp = []
    for e in exp:
        key = str(e[:2]) # state, action
        if not hashtable.has_key(key):
            hashtable[key] = 1
            new_exp.append(e)
    
    return new_exp    
################################################################################ 
def n_bits(scale):
    cnt = 0
    while (scale):
        scale = scale >> 1
        cnt += 1
    return cnt
################################################################################ 
def num2bitarray(num_list, bitlength):
    form = '{0:0%db}' % (bitlength)
    s = ''
    for n in num_list:
        s = s + form.format(n)
    ret = np.array(map(int, list(s)))
    return ret, len(ret)
################################################################################    
def data_scaling(matrix, scale):
    code_matrix = np.zeros((1, 2))
    code_matrix[0, 0] = matrix[0, 0]/float(3)
    code_matrix[0, 1] = matrix[0, 1]/float(50)
    # print "matrix", code_matrix
    return code_matrix
################################################################################    
def encode_condition(subdir, location, cond, scale, len_trace):
    code_matrix = np.zeros((1, 2))

    with open(os.path.join(subdir, 'pos_dic.dat'), 'rb') as fdat:
        pos_dic, pos_cnt = pickle.load(fdat)    

    if not pos_dic.has_key(location):
        pos_dic[location] = pos_cnt
        pos_cnt += 1
        # print "\n new position registered \n"
    pos = pos_dic[location]        

    with open(os.path.join(subdir, 'pos_dic.dat'), 'wb') as fdat:
        pickle.dump([pos_dic, pos_cnt], fdat) 
        
    code_matrix[0, 0] = pos        
    code_matrix[0, 1] = len_trace
    
    # print code_matrix
    
    return data_scaling(code_matrix, scale)
################################################################################
def get_state(history, n_history):

    cond_sz = len(history[-1][1][0,:])
    matrix = np.zeros((1,n_history, cond_sz+1))
    for i in xrange(n_history):
        if history[i] is None:
            continue
        # print 'hi:', history[i]
        matrix[0, i, 0:cond_sz] = history[i][1][0,:] # the larger i, the newer
        matrix[0, i, cond_sz] = history[i][0]*1 #action
        
    matrix[0, -1, cond_sz] = -1 #action
    
    # print "matrix", matrix
    return matrix.astype('float32')
################################################################################
def get_next_state(history, n_history):

    cond_sz = len(history[-1][1][0,:])
    matrix = np.zeros((1,n_history, cond_sz+1))
    for i in xrange(n_history-1):
        if history[i+1] is None:
            continue
        # print 'hi:', history[i]
        matrix[0, i, 0:cond_sz] = history[i+1][1][0,:] # the larger i, the newer
        matrix[0, i, cond_sz] = history[i+1][0]*1

    if history[-1][3] is not None:    
        matrix[0, -1, 0:cond_sz] = history[-1][3][0,:]
        matrix[0, -1, cond_sz] = -1
    
    return matrix.astype('float32')    
################################################################################
def proc2history(exp_pool, n_history):
    history_list = []
    level_list = []
    dummy = [[0,0,0,None]]*(n_history-1)
    exp_pool = dummy + exp_pool
    level = 1
    for i in xrange(len(exp_pool)-(n_history-1)):
        history = [None]*n_history
        history = exp_pool[i:(i+n_history)]

        for j in reversed(range(n_history-1)):
            if history[j][3] is None:
                for k in xrange(j+1):
                    history[k] = None
                break
        # print 'history', history
        history_list.append(history)   
        level_list.append(level)
        level += 1
        if history[-1][3] is None:
            level = 1        
        
    return history_list, level_list


################################################################################
def exe_tree(tree, histset, levelset, n_hist):
    
    for h, l in zip(histset, levelset):

        # print l, get_state(h, n_hist)
        # print l, get_state(h, n_hist, next = True)
        

        key = str(l)+str(get_state(h, n_hist))
        action = h[-1][0]
        last = True if (h[-1][3] is None) else False
        next_state = get_next_state(h, n_hist)
        # if h[-1][1][0,-1] == 4:
            # print h
        

        if not tree.has_key(key):
            tree[key] = [0, 0]
            
        # hold only one value for a key, ignoring multiple definitions        
        tree[key][action*1] = {
            'next_state': next_state,
            'last': last
        }
    return tree
################################################################################    