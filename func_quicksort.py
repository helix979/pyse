import numpy as np

def quicksort(A, aux = None):
    return np.sort(A, kind='quicksort')