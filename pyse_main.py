import pyse_learning as learn
import pyse_pathfinder as pf
import pyse_helper as helper
import pyse_config as config
import os, pickle
import datetime as dt

    
def execute(module, scale, aux, n_time, epsilon, action_seq = [], mode = 'run'):
    params = "%s %s %d %d %d %.4f " % (mode, module, scale, aux, n_time, epsilon)
    cmd = 'python pyse_program.py ' + params
    
    aseq_size = len(action_seq)
    values = ''.join( map(lambda x:' '+str(x*1), action_seq) )
    aseq_cmd = str(aseq_size) + values
    
    cmd = cmd + aseq_cmd
    # print 'cmd', cmd
    os.system(cmd)
#----------------------------------------------------------------------------------------------------
def main(param):
    ###################################
    max_n_exp = 5000
    n_execute = 10000
    learning_rate = 0.05
    pf.retry_max = 500
    ###################################
    exp_hashtable = {}
    exp_set = []
    best_exp = []
    action_seq = []
    max_path_len = 0
    try_till_max = 0
    num_unique_path = 0
    elapsed_till_max = 0    
    ###################################
    _, _, subdir =  config.getEnvVar(param['module'])
    result_file = os.path.join(subdir, 'result_' + param['module'] + '.dat')
    ###################################    

    
    dqn = learn.DQN(subdir, param['n_time'], n_batch = param['n_batch'], trained_model=param['trained_model_file'])
    upf = pf.PathFinder(subdir, param['n_time'])
    
    start_time = dt.datetime.now()
    for i in xrange(n_execute):    
        # symbolic execution
        print "\n-- Symbolic Execution --"
        execute(param['module'], param['scale'], param['aux'], param['n_time'], param['epsilon'], action_seq=action_seq)

        now = dt.datetime.now()
        elapsed = (now - start_time).seconds
            
        # hash experiences and find the new ones
        exp = helper.parse(subdir)
        path_len = len(exp)
        new_exp = helper.new_exp(exp_hashtable, exp)
        exp_set += new_exp
        
        # update the maximum length of a path
        if path_len > max_path_len:
            max_path_len = path_len
            best_exp = exp
            try_till_max = i+1
            elapsed_till_max = elapsed
            with open(result_file, 'wb') as fdat:
                pickle.dump([max_path_len, try_till_max, elapsed_till_max, (i+1), exp_set, exp_hashtable, best_exp], fdat)
                    
        if len(new_exp)>0:
            num_unique_path += 1
        if param['max_path_len_goal']>0:
            if max_path_len>=param['max_path_len_goal']:
                print 'max_path_len >= max_path_len_goal'
                return max_path_len, try_till_max, elapsed_till_max, (i+1)            

        # abandon old experiences when there are too many
        n_exp = len(exp_set)
        if n_exp > max_n_exp:
            dif = n_exp - max_n_exp
            print 'dif: ', dif
            del exp_set[:dif]
                
        print '-----------------------------------------------------------------'
        print 'i = %d, n_exp = %d, max_path_len = %s at the %d-th trial (%s)\n' % (i, len(exp_set), max_path_len, try_till_max, helper.minsec(elapsed_till_max))

        
        # update Q-network
        print "\n-- Policy Update --"
        dqn.load_data(exp_set, shuffle = True)
        n_epoch = 5 if n_exp < 100 else 2
        dqn.train(learning_rate, n_epoch)
        
        
        upf.update_tree(exp)
        action_seq = upf.find(param['epsilon'])
        
    return max_path_len, try_till_max, elapsed_till_max, n_execute            
#----------------------------------------------------------------------------------------------------    
if __name__ == '__main__':    
    param = {}
    
    param['n_batch'] = 1
    param['epsilon'] = 0.1    
    param['trained_model_file'] = None
    param['max_path_len_goal'] = -1
    
    param['module'] = 'sm'
    param['n_time'] = 3
    param['scale'] = 5
    param['aux'] = 0
    
    # param['module'] = 'boyer-moore'
    # param['n_time'] = 3
    # param['scale'] = 10
    # param['aux'] = 0
    # param['trained_model_file'] = './results_boyer-moore/q_model.dat'
    
    # param['module'] = 'dijkstar'
    # param['n_time'] = 3
    # param['scale'] = 20
    # param['aux'] = 5
    

    main(param)