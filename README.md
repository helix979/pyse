# PySE 

PySE is a stress testing tool that automatically finds out the worst-case execution path. PYSE uses symbolic execution to collect the behaviors of a given branching policy, and updates the policy using a reinforcement learning
approach through multiple executions. PYSE’s branching policy keeps evolving in a way that the length of an execution path increases in the long term, and ultimately reaches the worst-case complexity.

To cite PySE, please use the following paper:
```
Jinkyu Koo, Charitha Saumya, Milind Kulkarni and Saurabh Bagchi, "PySE: Automatic Worst-Case Test Generation by Reinforcement Learning," At the 12th IEEE International Conference on Software Testing, Verification, and Validation (ICST), Apr. 2019.
```

## Dependency
* Python (v2.7)
* Theano: Python ML library from `http://deeplearning.net/software/theano/`
* Z3Py: Z3 constraint solver (Python version) from `https://github.com/Z3Prover/z3`


## How to use
To analyze a program, execute the main file of Pyse, `pyse_main.py` as follows:

```
$ python pyse_main.py
```

Within the file, you can specify the program to analyze. In detail, you need to setup four parameters: `param['module']` (key-name of the program), `param['n_time']` (history length, i.e., `L` in the paper), `param['scale']` (the number of sysmbolic variables, i.e., the program input), and `param['aux']` (auxiliary variable that the program requires to define the scale). For example,

    param['module'] = 'sm'
    param['n_time'] = 3
    param['scale'] = 5
    param['aux'] = 0

the above means that we are going to analyze the program whose key-name is `sm` using the history legnth=3 at scale=5. Most of programs do not use `param['aux']`, and disregard it. Thus, you can set any value for `param['aux']` in such a case. However, some programs still use it, e.g., Dijkstra uses it to specify the number of nodes (the scale in this case is the number of edges).

The key-name is our internal key to find out the program. Such a mapping is defined in `config.ini`, where you will see:

    [sm]
    module_name: func_sm
    func_name: sm_wrapper

`[sm]` is the key-name, `module_name` is the Python file that contains the program to analyze, and `func_name` is the name of a function to execute in `module_name`. Every `func_name` should be a function that has take two argument as inputs. For example, `sm_wrapper` should like:

    def sm_wrapper(x, aux = None):

where `x` is the program input, and `aux` is for `param['aux']`. If the program does not have such an interface, you can simply make a wrapper.


Within `pyse_main.py`, you can also see other parameters like:


    param['epsilon'] = 0.1    
    param['trained_model_file'] = None
    param['max_path_len_goal'] = -1

Here, `param['epsilon']` is the value of the epsilon for the epsilon-greedy policy, `param['trained_model_file']` is the path of the Q-network model that are pre-trained (e.g., you can set as `param['trained_model_file']='./results_boyer-moore/q_model.dat'`). By default, `param['trained_model_file']=None`, which means that we do not use a pre-trained model. `param['max_path_len_goal']` is the goal of the path length to reach. You can use this to terminate the search early in case you know the maximum possible length. Setting `param['max_path_len_goal']=-1` means that you do not use this feature.


# LICENSE
PySE is available under the MIT license. See the `LICENSE.MIT` file for the details.
