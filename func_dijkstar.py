from dijkstar import Graph, find_path
import traceback

    
def dijkstar(A, n_node):    

    graph = Graph()
    
    scale = (n_node-1)*n_node/2
    cnt = 0
    for i in xrange(1,n_node+1):
        graph.add_edge(i, i, {'cost': 0})
        for j in xrange(i+1, n_node+1):
            graph.add_edge(i, j, {'cost': A[cnt]})
            graph.add_edge(j, i, {'cost': A[cnt]})
            cnt += 1
    assert cnt == scale

    cost_func = lambda u, v, e, prev_e: e['cost']
    try:
        find_path(graph, 1, n_node, cost_func=cost_func)    
    except BaseException as e:
        # print("******* %s stops by %s *******" % (__name__, repr(e)))
        # traceback.print_exc()
        # print 'exception'
        pass

def dijkstar_asym(A, n_node):    

    graph = Graph()
    
    scale = (n_node-1)*n_node
    cnt = 0
    for i in xrange(1,n_node+1):
        graph.add_edge(i, i, {'cost': 0})
        for j in xrange(i+1, n_node+1):
            graph.add_edge(i, j, {'cost': A[cnt]})
            cnt += 1            
            graph.add_edge(j, i, {'cost': A[cnt]})
            cnt += 1
    assert cnt == scale

    cost_func = lambda u, v, e, prev_e: e['cost']
    try:
        find_path(graph, 1, n_node, cost_func=cost_func)    
    except BaseException as e:
        # print("******* %s stops by %s *******" % (__name__, repr(e)))
        # traceback.print_exc()
        # print 'exception'
        pass        