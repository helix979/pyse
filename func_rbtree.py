# bintrees-2.0.7
import bintrees
 
def brtree_build(numbers):
    dymmy_const = 1
    t = bintrees.RBTree()
    for elm in numbers:
        t.insert(elm, dymmy_const)
    
    return t
    
def brtree_build_wrapper(A, aux = None):
    
    return brtree_build(A)