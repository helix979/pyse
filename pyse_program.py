import pyse_se as se
import pyse_config as config
import sys, os, pickle
import importlib

    

if __name__ == "__main__":
    params = {}
    params['mode'] = sys.argv[1]
    params['module'] = sys.argv[2]
    params['scale'] = int(sys.argv[3])
    params['aux'] = int(sys.argv[4])
    params['n_time'] = int(sys.argv[5])
    params['epsilon'] = float(sys.argv[6])    
    module_name, func_name, subdir =  config.getEnvVar(params['module'])            
    params['subdir'] = subdir
    
    action_seq = []
    size = int(sys.argv[7])
    for i in xrange(size):
        action_seq.append(int(sys.argv[8+i]))
    params['action_seq'] = action_seq    
    
    se.init(params)     
    A = se.BitVecVector("A", 32, params['scale'])
    
    func2exe = importlib.import_module(module_name).__getattribute__(func_name)
    func2exe(A, params['aux'])

    
    
    