import os
import random
import re
import pickle
import numpy as np

import theano
import theano.tensor as T

model_fname = 'q_model.dat'

#-----------------------------------------------------------------------------------------------------  
#-----------------------------------------------------------------------------------------------------  
def adam(loss, all_params, learning_rate=0.001, b1=0.9, b2=0.999, e=1e-8, gamma=1-1e-8):
    """
    ADAM update rules
    Default values are taken from [Kingma2014]
    References:
    [Kingma2014] Kingma, Diederik, and Jimmy Ba.
    "Adam: A Method for Stochastic Optimization."
    arXiv preprint arXiv:1412.6980 (2014).
    http://arxiv.org/pdf/1412.6980v4.pdf
    """
    updates = []
    all_grads = theano.grad(loss, all_params)
    alpha = learning_rate
    t = theano.shared(np.float32(1))
    b1_t = b1*gamma**(t-1)   #(Decay the first moment running average coefficient)

    for theta_previous, g in zip(all_params, all_grads):
        m_previous = theano.shared(np.zeros(theta_previous.get_value().shape,
                                            dtype=theano.config.floatX))
        v_previous = theano.shared(np.zeros(theta_previous.get_value().shape,
                                            dtype=theano.config.floatX))

        m = b1_t*m_previous + (1 - b1_t)*g                             # (Update biased first moment estimate)
        v = b2*v_previous + (1 - b2)*g**2                              # (Update biased second raw moment estimate)
        m_hat = m / (1-b1**t)                                          # (Compute bias-corrected first moment estimate)
        v_hat = v / (1-b2**t)                                          # (Compute bias-corrected second raw moment estimate)
        theta = theta_previous - (alpha * m_hat) / (T.sqrt(v_hat) + e) #(Update parameters)

        updates.append((m_previous, m))
        updates.append((v_previous, v))
        updates.append((theta_previous, theta) )
    updates.append((t, t + 1.))
    return updates
#-----------------------------------------------------------------------------------------------------  
#-----------------------------------------------------------------------------------------------------  
class Layer:
    def __init__(self, input, n_in, n_out, activation=None, W=None, b=None):

        rng = np.random.RandomState(1234)

        if W is None:
            W_values = np.asarray(
                rng.uniform(
                    low=-np.sqrt(6. / (n_in + n_out)),
                    high=np.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )
            W_values *= 4

            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = np.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)

        self.W = W
        self.b = b

        self.input = input
        lin_output = T.dot(input, self.W) + self.b
        self.output = (
            lin_output if activation is None
            else activation(lin_output)
        )

        self.params = [self.W, self.b]
#-----------------------------------------------------------------------------------------------------    
#-----------------------------------------------------------------------------------------------------  
class LSTMLayer:
    
    def __init__(self, input, n_in, n_out, n_time, n_batch = 1, params=None):
        """
            input is of shape (n_batch, n_time, n_in)
            output is of shape (n_batch, n_time, n_out)
        """
        n_batch = input.shape[0]
        
        self.Wx = theano.shared(
            0.2 * np.random.uniform(-1.0, 1.0, (n_in, n_out*4)).astype(theano.config.floatX)
        )
        self.Wh = theano.shared(
            0.2 * np.random.uniform(-1.0, 1.0, (n_out, n_out*4)).astype(theano.config.floatX)
        )
        self.b = theano.shared(
            np.zeros((n_out*4,), dtype=theano.config.floatX)
        )

        if params is not None:
            self.Wx, self.Wh, self.b = params[0], params[1], params[2]
        
        x_seq = input.dimshuffle(1,0,2)
        preact_x = T.dot(x_seq, self.Wx) + self.b
        
        def slice(preact, n):
                return preact[:, (n*n_out):((n+1)*n_out)]
            
        def recurrence(preact_x_, h_prev, c_prev):
            preact = preact_x_ + T.dot(h_prev, self.Wh)
            
            i = T.nnet.sigmoid(slice(preact, 0))
            f = T.nnet.sigmoid(slice(preact, 1))
            o = T.nnet.sigmoid(slice(preact, 2))
            c = T.tanh(slice(preact, 3))

            c = f * c_prev + i * c
            h = o * T.tanh(c)
            
            return h, c
            
        results, _ = theano.scan(
            fn = recurrence,
            sequences = [preact_x],
            outputs_info = [
                T.alloc(np.asarray(0., dtype=theano.config.floatX), n_batch, n_out),
                T.alloc(np.asarray(0., dtype=theano.config.floatX), n_batch, n_out)
            ],
            n_steps = n_time
        )
            
        self.input = input        
        self.output = results[0].dimshuffle(1,0,2)
        self.params = [self.Wx, self.Wh, self.b]
#-----------------------------------------------------------------------------------------------------    
#-----------------------------------------------------------------------------------------------------  
class Q:
    def __init__(self, input, n_in, n_feat, n_out, n_time, params=None):
    
        params0, params1 = None, [None, None]
        if params is not None:
            params0 = params[:3]
            params1 = params[3:]
            
        self.layer1 = LSTMLayer(
            input = input,
            n_in = n_in,
            n_out = n_feat,
            n_time = n_time,
            params = params0
        )
        
        self.layer2 = Layer(
            input = T.reshape(
                self.layer1.output,
                (-1, n_feat*n_time)
            ),
            n_in = n_feat*n_time,
            n_out = n_out,
            W = params1[0],
            b = params1[1]
        )
        
        self.input = input
        self.output = self.layer2.output
        self.params = self.layer1.params + self.layer2.params
#-----------------------------------------------------------------------------------------------------  
#-----------------------------------------------------------------------------------------------------  
class DQN:
    def __init__(self, subdir, n_time, gamma = .9, n_batch = 1, trained_model=None):
    
        self.subdir = subdir
        self.n_time = n_time
        self.n_input = 3
        self.n_output = 2
        self.n_batch = n_batch    
        self.gamma = gamma
        
        self.x = T.tensor3('x')
        self.y = T.matrix('y')

        params = None
        if trained_model is not None:
            q_trained = pickle.load(open(trained_model))
            params = q_trained.params

        self.q = Q(
            input = self.x,
            n_in = self.n_input,
            n_feat = 2,
            n_out = self.n_output,
            n_time = self.n_time,
            params = params
        )            
            
        self.qval = theano.function(
            inputs = [self.x],
            outputs = self.q.output
        )    

            
        with open(os.path.join(self.subdir, model_fname), 'wb') as f:
            pickle.dump(self.q, f)            
            
        self.cost_max = 0
        self.q_old = np.zeros((1,2))
        self.q_next_old = np.zeros((1,2))
        
#-----------------------------------------------------------------------------------------------------  
    def load_data(self, _exp_set, shuffle = True):
        exp_set = _exp_set[:]
        if shuffle == True:
            random.shuffle(exp_set)
            
        self.exp_set = exp_set
        self.n_sample = len(self.exp_set)
        self.n_iteration = int(self.n_sample/self.n_batch)
#-----------------------------------------------------------------------------------------------------      
    def proc_batch(self, idx):
        
        x_batch = np.zeros((self.n_batch, self.n_time, self.n_input))
        y_batch = np.zeros((self.n_batch, self.n_output))
        for i in xrange(self.n_batch):
            e = self.exp_set[i+self.n_batch*idx]
            state = e[0]
            reward = e[2]

            q_val = self.qval(np.expand_dims(state, axis=0))
            # print 'q_val: ', q_val
            
            val = 0
            if e[3] is not None:
                next_state = e[3]
                q_next_val = self.qval(np.expand_dims(next_state, axis=0))
                val = max(q_next_val.reshape(-1))

            new_val = reward + self.gamma*val
             
            q_val[0, e[1]*1] = new_val
            
            x_batch[i,:,:] = state
            y_batch[i,:] = q_val[0,:]
        
        return x_batch.astype('float32') , y_batch.astype('float32') 
    
#-----------------------------------------------------------------------------------------------------      
    def train(self, learning_rate, n_epoch = 1):
                    

        adif = T.mean(T.abs_(self.y - self.q.output))
        cost = adif
        
        updates = adam(cost, self.q.params, learning_rate)

        
        train_model = theano.function(
            inputs = [self.x, self.y],
            outputs = [cost, self.q.output],
            updates = updates
        )
        
        for e in xrange(n_epoch):
            random.shuffle(self.exp_set)
            for i in xrange(self.n_iteration):
                x_batch, y_batch = self.proc_batch(i)
                
                _cost, _q = train_model(
                    x_batch,
                    y_batch
                )
                    
                if i % 100 == 0:
                    print "%d:%d\t%.3f\t%s" %(e, i, _cost, str(_q[0,:]))
                    
        with open(os.path.join(self.subdir, model_fname), 'wb') as f:
            pickle.dump(self.q, f)
        # print "------------------------------------"
#-----------------------------------------------------------------------------------------------------  
#-----------------------------------------------------------------------------------------------------  
class DQN_pred:
    def __init__(self, subdir, n_time, action_seq = []):
        
        self.n_time = n_time    
        
        dqn = pickle.load(open(os.path.join(subdir, model_fname)))
        self.q = theano.function(
            inputs = [dqn.input],
            outputs = dqn.output
        )        
        self.action_seq = action_seq
#-----------------------------------------------------------------------------------------------------      
    def predict(self, t, state, epsilon = 0, printout = True):
        
        if len(self.action_seq)>0:
            a = (self.action_seq[0] == 1)
            a_mark = 'UPF'
            del self.action_seq[0]
            
            if printout:
                print "%d%s\t%s" % (t, a_mark, str(a))
        else:
            # print 'state.dtype: ', state.dtype
            q_val = self.q(np.expand_dims(state, axis=0))
            
            if q_val[0,0] < q_val[0,1]:
                a = True
            else:
                a = False

            a_mark = ''            
            if (random.random() <= epsilon):                
                a = random.choice([True, False]) 
                a_mark = 'R'
                
            if printout:
                print "%d%s\t%s\t%.3f\t%.3f" % (t, a_mark, str(a), q_val[0,0], q_val[0,1])
       
        return a
#-----------------------------------------------------------------------------------------------------                                 
            
