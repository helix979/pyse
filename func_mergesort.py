import numpy as np

def mergesort(A, aux = None):
    return np.sort(A, kind='mergesort')