import numpy as np

class ScoreParam:
    """The parameters for an alignment scoring function"""
    def __init__(self, gap=-7, match=10, mismatch=-5):
        self.gap = gap
        self.match = match
        self.mismatch = mismatch

def local_align(x, y, score=ScoreParam(-5, 1, -1)):
    """Do a local alignment between x and y"""

    A = np.zeros((len(x) + 1, len(y) + 1))

    best = 0
    optloc = (0,0)
   # fill in A in the right order
    for i in xrange(1, len(x)+1):
        for j in xrange(1, len(y)+1):
            # the local alignment recurrance rule:
            A[i][j] = max(
               A[i][j-1] + score.gap,
               A[i-1][j] + score.gap,
               A[i-1][j-1] + (score.match if x[i-1] == y[j-1] else score.mismatch),
               0
            )
            # track the cell with the largest score
            if A[i][j] >= best:
                best = A[i][j]
                optloc = (i,j)
                # print i-1,j-1, x[i-1], y[j-1]
    # return the opt score and the best location
    return best, optloc, x[:optloc[0]], y[:optloc[1]]
 
def sm_wrapper(x, aux = None): 
    local_align(x, [1,2,3], score=ScoreParam(-5, 1, -1))
        
if __name__ == '__main__':
    print local_align("AGCGTAG", "CTCGTC")        
    print local_align("bestoftimes", "soften")