import pickle
import numpy as np
import pyse_learning as learn

retry_max = 200

class PathFinder:
    def __init__(self, subdir, n_time):
        self.tree = {}
        self.dqn = learn.DQN_pred(subdir, n_time)

    def update_tree(self, exp):
    
        self.start_state = exp[0][0]
        
        for t, e in enumerate(exp):
            key = str(t) + ":" + str(e[0])
            action = e[1]
            last = True if (e[3] is None) else False
            next_state = e[3]

            if not self.tree.has_key(key):
                self.tree[key] = [0, 0]

            # hold only one value for a key, ignoring multiple definitions        
            self.tree[key][action*1] = {
                'next_state': next_state,
                'last': last
            }

    def find(self, epsilon_given = 0):
        t = 0
        action_list = []
        state = self.start_state
        retry_cnt = 1
        epsilon = epsilon_given
        
        while True:            
            action = self.dqn.predict(t, state, epsilon, False)
            # print 'action', t, action
            action_list.append(action)
            key = str(t) + ":" + str(state)
            t += 1
            # print key    
            # print hash_table[str(state)+str(action)]
            if self.tree.has_key(key):
                if self.tree[key][action*1] != 0:
                    if self.tree[key][action*1]['last'] == False:
                        state = self.tree[key][action*1]['next_state']
                    else:                        
                        # print '\n path ended, retry %d' % (retry_cnt)
                        retry_cnt += 1
                        t = 0
                        action_list = []                
                        state = self.start_state                        

                        if retry_cnt == retry_max/2:
                            epsilon = 2.0*epsilon_given
                        if retry_cnt == retry_max:
                            break
                        
                else:
                    print '\nUPF: Found a new branch decision (trial=%d, epsilon=%.3f)' %(retry_cnt, epsilon)
                    break
                    
            else:
                # Will not come to here, though.
                print '\nFound a new branch condition'
                break
                
                
        print action_list        
        
        return action_list        
