import numpy as np

def isort(A):
	
	for i in range(1, len(A)):
		x = A[i]
		j = i -1
		while(j >= 0 and A[j] > x):
			A[j+1] =  A[j]
			j = j - 1

		A[j+1] = x

	return A
    
def isort_wrapper(A, aux = None):
    # isort(A)
    # sorted(A)
    # np.sort(A, kind='mergesort')
    
    return isort(A)