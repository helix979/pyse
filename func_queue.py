import Queue

# https://docs.python.org/2/library/queue.html

def pqueue(numbers, aux = None):
    q = Queue.PriorityQueue()
    for elm in numbers:
        q.put(elm)    
        
    return q
