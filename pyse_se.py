from z3 import *
import os, re
import sys
import atexit
import traceback
import random
import pickle
import numpy as np
# import pyse2_dqn_encode as enc
import pyse_learning as learn

#####################################################################
solver = Solver()
trace = []
conditions = []
location = []
state_t = []
filename = None
params = {}
dqn = None
h = None
#####################################################################
def init(_params):
    global params, filename, h
    params = _params
    filename = os.path.join(params['subdir'], 'path_log.dat')
    
    if os.path.isfile(filename):
        os.remove(filename)
        
    if params['mode'] == 'run':
        global dqn
        dqn = learn.DQN_pred(params['subdir'], params['n_time'], params['action_seq'])
        
    h = [(-1,-1) for i in range(params['n_time'])]
#####################################################################            
def save_data(data):
    with open(filename, 'wb') as fdat:
        pickle.dump(data, fdat)              
#####################################################################            
def make_state(key):
    n_time = params['n_time']
    t = len(location)-1 # the t-th branch condition
    addition = []
    for i in reversed(range(n_time)):
        addition.append((t-i,))
    state = [i+j for i,j in zip(addition, key)] # (t, loc, action) 
    return np.asarray(state).astype('float32') # shape=(3,n_time) # Here, True and False change to 1 and 0, respectively.
######################################################################    
def check_cond(self):
    conditions.append(self)
    loc = traceback.extract_stack(limit=2)[0][1]
    location.append(loc)
    t = len(location)-1
    pair = (int(loc), -1)
    h.pop(0)
    key = tuple(h + [pair])

        
    state = make_state(key)
    # print 'state: ', state
    
    if params['mode'] == 'random':
        action = (random.randint(0,1) == 1)
    elif params['mode'] == 'run':          
        action = dqn.predict(t, state, params['epsilon'])
            
    if action:
        solver.add(self)
    else:
        solver.add(Not(self))
        
    pair2 = (int(loc), action)
    h.append(pair2[:])

    trace.append([state, action, t, 1]) # the last is feasibility
    
    if solver.check() != sat:
        sys.exit(0)
        
    return action
#####################################################################
def mc_exit():
    feasible = 1
    ret = solver.check()
    if ret != sat:
        feasible = 0
        
    trace[-1][3] = feasible
        
    save_data(trace)
    
    print "feasibility=%d, path_length=%d" % (feasible, len(location))
    
#####################################################################
setattr(BoolRef, "__nonzero__", check_cond)
atexit.register(mc_exit)
######################################################################
def BitVecVector(prefix, sz, N):
    """Create a vector with N Bit-Vectors of size sz"""
    return [ BitVec('%s_%s' % (prefix, i), sz) for i in range(N) ]
######################################################################
